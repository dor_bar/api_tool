import json
import sys

import datetime
import requests
import web

from cb import CouchBase


# CouchBase configuration:
CB_HOST_Prod = 'cb01-usa4-prod.idmadm.com'
CB_HOST_Pre_Prod = 'cb01-usa4-prod.idmadm.com'
CB_PASSWORD_Prod = "Pr0dLimit3d"
# cb = CouchBase(CB_HOST, CB_PASSWORD, BUCKET_NAME)

# Web application connfiguration:
render = web.template.render('templates/')
urls = (
    '/', 'index',
    '/api_tool', 'index',
    "/request_history", "request_history",
    "/save_request", "save_request"
)
app = web.application(urls, globals())
CD_RENDERING = None
CB_MONITOR = None


# session = web.session.Session(app, web.session.DiskStore('sessions'), initializer={'count': 0})


def get_auth_token(account_id, cb_connection):
    doc = cb_connection.get_doc("account_credentials_" + str(account_id), quiet=True)
    if doc is not None:
        return doc["api"]["secretKey"]
    else:
        return None


def get_db_renderting():
    global CD_RENDERING
    if CD_RENDERING is None:
        CD_RENDERING = CouchBase("cb01-usa-pre-prod.idmadm.com", "Pr0dLimit3d", 'rendering')
    return CD_RENDERING

def get_db_monitor():
    global CB_MONITOR
    if CB_MONITOR is None:
        CB_MONITOR = CouchBase("cb01-usa-pre-prod.idmadm.com", "Pr0dLimit3d", "monitor")
    return CB_MONITOR

class index:
    def GET(self):
        return render.index()

    def POST(self):
        cb_rendering = get_db_renderting()
        data = json.loads(web.data())
        auth_token = get_auth_token(data["account_id"], cb_rendering)
        api_server = data["url"]
        headers = data["headers"]
        request = data["request_data"]

        if auth_token is not None:
            res = requests.post(api_server[1],
                                data=request.encode('UTF-8'),
                                headers=headers,
                                auth=(data["account_id"], auth_token))
        else:
            return web.HTTPError("Account not exists.")
        if res.status_code == requests.codes.ok:
            return json.dumps(res.json())


class request_history:
    def GET(self):
        cb_monitor = get_db_monitor()
        history_list = cb_monitor.get_doc("PASTRAMA_Requests")
        history_list["history"].reverse()
        name_list = []
        for request in history_list["history"]:
            name_list.append(request["request_name"])
        return name_list

    def POST(self):
        data = web.data()
        cb_monitor = get_db_monitor()
        history_list = cb_monitor.get_doc("PASTRAMA_Requests")
        request_name = data.decode('utf-8')
        for request in history_list["history"]:
            if request["request_name"] == request_name:
                return json.dumps(request)
        return "{'error': 'Request is not available!'}"


class save_request:
    def POST(self):
        raw_data = json.loads(web.data())
        data = raw_data
        cb_monitor = get_db_monitor()
        history_list = cb_monitor.get_doc("PASTRAMA_Requests")
        history_list["history"].append({
            "request_name": data["request_name"],
            "request": data["request"],
            "last_use": datetime.datetime.now().strftime("%Y/%m/%d-%H:%M"),
            "last_response": "",
            "configuration": data["configuration"]
        })
        cb_monitor.create_doc("PASTRAMA_Requests", history_list)
        return web.Accepted


if __name__ == "__main__":
    app.run()
