#!/usr/bin/python
# coding=utf-8
"""
Author:  talm
Created on 1/26/17
All Rights reserved to IDOMOO.INC 2013

"""
from couchbase.bucket import Bucket, NotFoundError
from couchbase.views.iterator import View
from couchbase.views.params import Query


class CouchBase(object):
    """
        couchbase connection
    """

    def __init__(self, host, password, bucket_name):
        self.bucket_name = bucket_name
        self.password = password
        self.host = host
        self.conn = None
        self.bucket = None  # type: Bucket
        self._connect()

    def _connect(self):
        self.bucket = Bucket('couchbase://{0}/{1}'.format(self.host, self.bucket_name), password=self.password)

    def get_doc(self, doc_id, quiet=False):
        """
        return document json
        :param quiet:
        :param doc_id:
        :return:
        """
        try:
            return self.bucket.get(doc_id).value
        except NotFoundError:
            if quiet is True:
                return None
            raise

    def create_doc(self, doc_id, data):
        """
        create doc
        :param doc_id:
        :param data:
        """
        return self.bucket.upsert(doc_id, data).value

    def get_counter(self, doc_id, delta=1):
        """
        return counter next num and increment
        :param delta:
        :param doc_id:
        :return:
        """
        return self.bucket.counter(doc_id, delta).value

    def view_query(self, design, name, query):
        view = View(self.bucket, design, name, include_docs=True, query=query)
        return view

    def bulk_delete(self, keys, quiet=False):
        return self.bucket.remove_multi(keys, quiet)